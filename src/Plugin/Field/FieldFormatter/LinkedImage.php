<?php
/**
 * @file
 * Contains Drupal\image_link_formatter\Field\FieldFormatter\LinkedImage
 */

namespace Drupal\image_link_formatter\Plugin\Field\FieldFormatter;

use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @FieldFormatter(
 *   id = "linked_image",
 *   label = @Translation("Linked Image"),
 *   field_types = {
 *     "image"
 *   }
 * );
 */
class LinkedImage extends FormatterBase implements ContainerFactoryPluginInterface {

  var $entityManager = NULL;

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    if (count($items) === 0) {
      return [];
    }

    $settings = $this->getSettings();
    $entity = $items[0]->getEntity();

    if ($settings['link_field'] === 'entity') {
      $url = $entity->url();
    }
    else {
      if ($field = $entity->{$settings['link_field']}->get(0)) {
        $url = $field->getUrl();
      }
      else {
        $url = NULL;
      }
    }

    $elements = [];
    foreach ($items as $delta => $item) {
      $item_attributes = $item->_attributes;
      unset($item->_attributes);
      $elements[$delta] = [
        '#theme' => 'image_formatter',
        '#item' => $item,
        '#item_attributes' => $item_attributes,
        '#image_style' => $settings['image_style'],
        '#url' => $url,
      ];
    }
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $form['link_field'] = [
      '#title' => $this->t('Link Field'),
      '#type' => 'select',
      '#options' => $this->getLinkFieldOptions(),
      '#default_value' => $settings['link_field'],
    ];
    $form['image_style'] = [
      '#title' => $this->t('Image Style'),
      '#type' => 'select',
      '#options' => image_style_options(),
      '#default_value' => $settings['image_style'],
    ];
    return $form;
  }

  /**
   * Get an options array of link fields.
   *
   * @return array
   *   An array of fields keyed by id.
   */
  public function getLinkFieldOptions() {
    $fields = $this->getLinkFields();
    $options = [
      'entity' => 'Link to Entity',
    ];
    foreach ($fields as $field) {
      $options[$field->getName()] = $field->getLabel();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return ['link_field' => 'entity', 'image_style' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [$this->t('Linked to @field', array('@field' => $settings['link_field']))];
    if ($settings['image_style'] !== '') {
      $summary[] = $this->t(', using image style "@style"', array('@style' => $settings['image_style']));
    }

    return ['#markup' => implode($summary, '')];
  }

  /**
   * Get all of the link fields which are attached to this entity and bundle.
   *
   * @return FieldDefinitionInterface[]
   *   An array of link field definitions.
   */
  public function getLinkFields() {
    return array_filter($this->entityManager->getFieldDefinitions($this->fieldDefinition->getTargetEntityTypeId(), $this->fieldDefinition->getTargetBundle()), function (FieldDefinitionInterface $element) {
      return $element->getType() === 'link';
    });
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, EntityManagerInterface $entity_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->entityManager = $entity_manager;
  }
}
