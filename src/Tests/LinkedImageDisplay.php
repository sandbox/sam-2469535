<?php
/**
 * @file
 * Contains Drupal\image\Tests\ImageFieldTestBase\ImageFieldTestBase.
 */

namespace Drupal\image_link_formatter\Tests;

use Drupal\image\Tests\ImageFieldTestBase;
use Drupal\link\LinkItemInterface;

/**
 * Tests that the linked and wrapped formatter works.
 *
 * @group image_link_formatter
 */
class LinkedImageDisplay extends ImageFieldTestBase {

  public static $modules = array('field_ui', 'image_link_formatter', 'link');

  /**
   * Test the linked and wrapped formatter works.
   */
  function testLinkedAndWrappedImage() {
    // Create an image and link field.
    $link_field_name = $this->createLinkField();
    $image_field_name = strtolower($this->randomMachineName());
    $this->createImageField($image_field_name, 'article', array('uri_scheme' => 'public'), array('alt_field_required' => 0));

    // Upload an image to a new node.
    $test_image = current($this->drupalGetTestFiles('image'));
    $nid = $this->uploadNodeImage($test_image, $image_field_name, 'article');
    $node_storage = $this->container->get('entity.manager')->getStorage('node');
    $node = $node_storage->load($nid);
    $file = file_load($node->$image_field_name->get(0)
      ->getValue()['target_id']);

    // Test the formatter when the link field is empty.
    $output = $this->renderField($node, $image_field_name, array(
      'type' => 'linked_image',
      'settings' => array('link_field' => $link_field_name)
    ));
    $this->assertEqual($output, '<img src="' . $file->url() . '" width="40" height="20" alt="" />');

    // Add a link to the node and test the output has been updated.
    $node->$link_field_name = array(
      'uri' => 'http://example.com',
      'options' => array(),
      'title' => 'Custom Link'
    );
    $node->save();

    // Test the formatter when a link is present.
    $output = $this->renderField($node, $image_field_name, array(
      'type' => 'linked_image',
      'settings' => array('link_field' => $link_field_name)
    ));
    $this->assertEqual($output, '<a href="http://example.com"><img src="' . $file->url() . '" width="40" height="20" alt="" /></a>');
  }

  /**
   * Render a field on a given entity.
   *
   * @param $entity
   *   The entity to render the field from.
   * @param $field_name
   *   The field to render.
   * @param $settings
   *   An array of field settings or a view mode.
   *
   * @return string
   *   Rendered field HTML.
   */
  public function renderField($entity, $field_name, $settings) {
    $build = $entity->get($field_name)->view($settings);
    drupal_render($build[0]);
    $field_output = trim($build[0]['#markup']);
    $field_output = str_replace("\n", '', $field_output);
    return $field_output;
  }

  /**
   * Create a link field to leverage for the test.
   */
  function createLinkField() {
    $field_name = strtolower($this->randomMachineName());
    // Create a field with settings to validate.
    $this->fieldStorage = entity_create('field_storage_config', array(
      'field_name' => $field_name,
      'entity_type' => 'node',
      'type' => 'link',
    ));
    $this->fieldStorage->save();
    $this->field = entity_create('field_config', array(
      'field_storage' => $this->fieldStorage,
      'bundle' => 'article',
      'settings' => array(
        'title' => DRUPAL_DISABLED,
        'link_type' => LinkItemInterface::LINK_GENERIC,
      ),
    ));
    $this->field->save();
    return $field_name;
  }

}
